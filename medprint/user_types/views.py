import json
from django.shortcuts import render, get_object_or_404, redirect, HttpResponseRedirect
from django.views.generic import CreateView, DetailView, UpdateView, ListView, DeleteView
from .models import *
from .forms import *
from geopy.geocoders import Nominatim
from django.contrib import messages

geolocator = Nominatim(user_agent="django_frontend")


class CommonCreate(CreateView):
    model = ''
    template_name = 'base/create.html'
    form = BaseModelCreateForm
    fields = [
        'name',
        'address_search',
        'location',
        'email',
        'phone',
        'url',
        'user_notes'
    ]
    success_url = "/"

    def post(self, request, *args, **kwargs):
        form_data = self.form(request.POST)
        new_model = form_data.save(commit=False)
        address_data = geolocator.reverse(new_model.location, addressdetails=True)
        address_json = json.dumps(address_data.raw)
        new_model.address_json = address_json
        new_model.user = self.request.user
        new_model.save()
        redirect_url = '/user_types/' + self.url_string + '/'+str(new_model.pk)
        return HttpResponseRedirect(redirect_url)


class MakerCreate(CommonCreate):
    form = MakerCreateForm
    model = Maker
    fields = [
        'name',
        'address_search',
        'location',
        'email',
        'phone',
        'url',
        'address_json',
    ]
    fields.append('equipment')
    url_string = 'makers'


class HospitalCreate(CommonCreate):
    form = HospitalCreateForm
    model = Hospital
    url_string = 'hospitals'


class DelivererCreate(CommonCreate):
    form = DelivererCreateForm
    model = Deliverer
    url_string = 'deliverers'


class DispatcherCreate(CommonCreate):
    form = DispatcherCreateForm
    model = Dispatcher
    url_string = 'dispatchers'


class CommonDetail(DetailView):
    template_name = "base/detail.html"


class MakerDetail(CommonDetail):
    model = Maker


class HospitalDetail(CommonDetail):
    model = Hospital


class DelivererDetail(CommonDetail):
    model = Deliverer


class DispatcherDetail(CommonDetail):
    model = Dispatcher


class CommonList(ListView):
    paginate_by = 10


class MakerList(CommonList):
    template_name = "lists/list_makers.html"
    model = Maker


class HospitalList(CommonList):
    template_name = "lists/list_hospitals.html"
    model = Hospital


class DelivererList(CommonList):
    template_name = "lists/list_deliverers.html"
    model = Deliverer


class DispatcherList(CommonList):
    template_name = "lists/list_dispatchers.html"
    model = Dispatcher
