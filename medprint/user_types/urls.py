from django.urls import include, path
from .views import *

app_name = 'user_types'

urlpatterns = [
    # path("base/create/", BaseModelCreate.as_view(), name="new"),
    # path("base/<int:pk>/", BaseModelDetail.as_view(), name="detail"),
    path("makers/new/", MakerCreate.as_view(), name="maker_new"),
    path("makers/<int:pk>/", MakerDetail.as_view(), name="maker_detail"),
    path("makers/", MakerList.as_view(), name="maker_list"),
    path("hospitals/new/", HospitalCreate.as_view(), name="hospital_new"),
    path("hospitals/<int:pk>/", HospitalDetail.as_view(), name="hospital_detail"),
    path("hospitals/", HospitalList.as_view(), name="hospital_list"),
    path("deliverers/new/", DelivererCreate.as_view(), name="deliverer_new"),
    path("deliverers/<int:pk>/", DelivererDetail.as_view(), name="deliverer_detail"),
    path("deliverers/", DelivererList.as_view(), name="deliverer_list"),
    path("dispatchers/new/", DispatcherCreate.as_view(), name="dispatcher_new"),
    path("dispatchers/<int:pk>/", DispatcherDetail.as_view(), name="dispatcher_detail"),
    path("dispatchers/", DispatcherList.as_view(), name="dispatcher_list"),
]
