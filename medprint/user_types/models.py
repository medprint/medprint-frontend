import json
from django.db import models
from location_field.models.plain import PlainLocationField
from phone_field import PhoneField
from config.settings.base import AUTH_USER_MODEL
from django import forms
from datetime import datetime
from django.contrib.postgres.fields import JSONField

user_types = (
    ('hospital', 'Hospital'),
    ('maker', 'Maker'),
    ('deliverer', 'Deliverer'),
)


# class BaseModel(models.Model):
# name = models.CharField(max_length=250)
# user_type = models.CharField(choices=user_types, null=True, default="hospital", max_length=20)
# phone = PhoneField(null=True, blank=True)
# email = models.EmailField(max_length=100, null=True, blank=True)
# url = models.URLField(max_length=200, blank=True)
# user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE,
# related_name="user_types", null=True, blank=True)

# address_search = models.CharField(
# max_length=250, help_text="Start typing your address or name of your business, and stop when it shows up on the map. We'll reverse engineer your address from there")
# location = PlainLocationField(based_fields=['address_search'], zoom=7, null=True, blank=True)

# address_data = models.CharField(max_length=500, default='', null=True, blank=True)

# def __str__(self):
# return self.name


class CommonUser(models.Model):
    name = models.CharField(max_length=250)
    phone = PhoneField(null=True, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)
    url = models.URLField(max_length=200, blank=True)

    address_search = models.CharField(
        max_length=250, help_text="Start typing your address or name of your business, and stop when it shows up on the map. We'll reverse engineer your address from there")
    location = PlainLocationField(based_fields=['address_search'], zoom=7, null=True, blank=True)
    address_json = JSONField(blank=True, null=True, default=dict)

    user_notes = models.TextField(blank=True, null=True)  # Notes the user makes
    staff_notes = models.TextField(blank=True, null=True)  # Notes staff can make about user

    status_verified = models.BooleanField(default=False, blank=True, null=True)
    status_active = models.BooleanField(default=True, blank=True, null=True)

    timestamp_create = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    timestamp_update = models.DateTimeField(auto_now=True, null=True, blank=True)

    @property
    def address_detail(self):
        data = json.loads(self.address_json)
        return data

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class Maker(CommonUser):

    equipment_choices = (
        ('3d_printer', '3D printer'),
        ('laser_cutter', 'Laser cutter'),
    )
    equipment = models.CharField(choices=equipment_choices, max_length=250, null=True, blank=True)

    equipment_list = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=equipment_choices)

    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE,
                             related_name="profile_maker", null=True, blank=True)


class Hospital(CommonUser):
    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE,
                             related_name="profile_hospital", null=True, blank=True)


class Dispatcher(CommonUser):
    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE,
                             related_name="profile_dispatcher", null=True, blank=True)


class Deliverer(CommonUser):
    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE,
                             related_name="profile_deliverer", null=True, blank=True)
    delivery_vehicle = models.TextField(blank=True, null=True)  # Vehicle info - how much can they carry, etc?
    delivery_area = models.TextField(blank=True, null=True)
