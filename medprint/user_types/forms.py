from django import forms
from django.forms import ModelForm
from .models import *


class CommonCreateForm(ModelForm):

    class Meta:
        model = ''
        exclude = []


class BaseModelCreateForm(ModelForm):

    class Meta:
        model = Maker
        exclude = []


class MakerCreateForm(CommonCreateForm):

    class Meta(CommonCreateForm.Meta):
        model = Maker


class HospitalCreateForm(CommonCreateForm):

    class Meta(CommonCreateForm.Meta):
        model = Hospital


class DelivererCreateForm(CommonCreateForm):

    class Meta(CommonCreateForm.Meta):
        model = Deliverer


class DispatcherCreateForm(CommonCreateForm):

    class Meta(CommonCreateForm.Meta):
        model = Dispatcher
