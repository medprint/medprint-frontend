from django.contrib import admin
from .models import *


# @admin.register(BaseModel)
# class BaseModelAdmin(admin.ModelAdmin):
# class Meta:
# list_display = 'name'


@admin.register(Maker)
class MakerAdmin(admin.ModelAdmin):
    class Meta:
        list_display = 'name'


@admin.register(Dispatcher)
class DispatcherAdmin(admin.ModelAdmin):
    class Meta:
        list_display = 'name'


@admin.register(Deliverer)
class DelivererAdmin(admin.ModelAdmin):
    class Meta:
        list_display = 'name'


@admin.register(Hospital)
class HospitalAdmin(admin.ModelAdmin):
    class Meta:
        list_display = 'name'
