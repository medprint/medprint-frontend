from django.template import Library, Node
from user_types.models import *

register = Library()


@register.inclusion_tag('embeds/latest_list.html')
def show_latest_makers(count=5):
    objects = Maker.objects.all()
    return {'objects': objects}


@register.inclusion_tag('embeds/latest_list.html')
def show_latest_hospitals(count=5):
    objects = Hospital.objects.all()
    return {'objects': objects}


@register.inclusion_tag('embeds/latest_list.html')
def show_latest_deliverers(count=5):
    objects = Deliverer.objects.all()
    return {'objects': objects}


@register.inclusion_tag('embeds/latest_list.html')
def show_latest_dispatchers(count=5):
    objects = Dispatcher.objects.all()
    return {'objects': objects}
