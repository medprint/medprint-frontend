# Generated by Django 3.0.4 on 2020-04-12 03:24

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import location_field.models.plain
import phone_field.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('user_types', '0005_auto_20200412_0223'),
    ]

    operations = [
        migrations.CreateModel(
            name='Maker',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('user_type', models.CharField(choices=[('hospital', 'Hospital'), ('maker', 'Maker'), ('deliverer', 'Deliverer')], default='hospital', max_length=20, null=True)),
                ('phone', phone_field.models.PhoneField(blank=True, max_length=31, null=True)),
                ('email', models.EmailField(blank=True, max_length=100, null=True)),
                ('url', models.URLField(blank=True)),
                ('address_search', models.CharField(help_text="Start typing your address or name of your business, and stop when it shows up on the map. We'll reverse engineer your address from there", max_length=250)),
                ('location', location_field.models.plain.PlainLocationField(blank=True, max_length=63, null=True)),
                ('address_data', models.CharField(blank=True, default='', max_length=500, null=True)),
                ('equipment', models.CharField(choices=[('3d_printer', '3D printer'), ('laser_cutter', 'Laser cutter')], default=True, max_length=250, null=True)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_type', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
