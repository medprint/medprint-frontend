from datetime import datetime
import gitlab
import os
from pprint import pprint
# from . personal_access_token import *

host = os.environ.get("GITLAB_HOST")
group_id = os.environ.get("GITLAB_GROUP_ID")
group_id = os.environ.get("GITLAB_GROUP_ID")


class GitlabUser:
    """
    Tools for creating and querying Gitlab users. The user running these functions requires admin status on Gitlab
    """
    def create_user(token, email, password, username, name):
        gl = gitlab.Gitlab(host, private_token=token)

        user = gl.users.create(
            {
                'email': email,
                'password': password,
                'username': username,
                'name': name
            }
        )


class GitlabProject:
    def get_projects(group_id=group_id):
        archived = False
        visibility = 'public'
        order_by = 'created_at'

        Debug.output(host, group_id)
        gl = gitlab.Gitlab(host)

        group = gl.groups.get(group_id, lazy=True)
        projects = group.projects.list(archived=False, visibility='public', order_by='created_at')

        return projects

    def get_project(project_id):
        gl = gitlab.Gitlab(host)

        return gl.projects.get(project_id)

    def new_project(token, group_id, name):
        gl = gitlab.Gitlab(host, private_token=token)

        return gl.projects.create(
            {
                'name': name,
                'namespace_id': group_id
            }
        )

    def get_file(token, project_id, file_path, branch):
        gl = gitlab.Gitlab(host, private_token=token)

        project = gl.projects.get(project_id)
        f = project.files.get(file_path, ref='master')

        # currently returns base64 encoded
        return f

    def new_file(token):
        gl = gitlab.Gitlab(host, private_token=token)

        project = gl.projects.get(46)  # Test repo
        f = project.files.create({
            'file_path': 'testfile.txt',
            'branch': 'master',
            'content': 'testing 123',
            'author_email': 'foo@bar.com',
            'author_name': 'Alex',
            'encoding': 'text',
            'commit_message': 'test commit'
        })


class GitlabTools:

    library_id = group_id

    def get_group(token, self, id):
        gl = gitlab.Gitlab(host, private_token=token)

        library = gl.groups.get(group_id)

    def get_designs_list(token, library_id=group_id):
        gl = gitlab.Gitlab(host, private_token=token)

        library = gl.groups.get(library_id, lazy=True)
        choices = []
        for item in library.projects.list():
            if (item.visibility == 'public') and (item.archived == False) and (item.empty_repo == False):
                choice = (item.id, item.name)
                choices.append(choice)

        return choices

    def create_issue(*args, **kwargs):
        gl = gitlab.Gitlab(host, private_token=kwargs["token"])

        project_id = kwargs["project_id"]
        print("project_id=")
        print(project_id)
        project = gl.projects.get(project_id, lazy=True)

        issue = project.issues.create(
            {
                'title': kwargs["title"],
                'description': kwargs["description"],
            }
        )

        issue.labels = kwargs["labels"]
        issue.task_completion_status["count"] = 5
        issue.save()
        return issue

    def create_issue_from_dict(token, data, labels=['basic'], files=None):
        gl = gitlab.Gitlab(host, private_token=token)

        project = gl.projects.get(data["project_id"], lazy=True)
        data = data.copy()  # Copy bc Original data is immutable
        data["labels"] = labels

        if 'New Part' in labels:
            print(data)
            data["description"] += f"\n\n## New Part Info\n\n**URL**: {data['url']}"

        if 'Print Request' in labels:
            data['description'] += f"\n\n## Print Request Info\n\n"

        issue = project.issues.create(data)
        issue.save()

        return issue

    def get_issues_by_user(*args, **kwargs):
        gl = gitlab.Gitlab(host, private_token=kwargs["token"])

        if "labels" in kwargs:
            labels = kwargs["labels"]
        else:
            labels = []

        username = kwargs["username"]

        if labels:
            issues = gl.issues.list(author_username=username, labels=labels, lazy=True).copy()
        else:
            issues = gl.issues.list(author_username=username, lazy=True).copy()

        issues_list = []
        for issue in issues:
            new_issue = issue.__dict__["_attrs"]

            # Add metadata like timedelta to pass to the template
            new_issue["updated_delta"] = ''  # Human readable updated timedelta
            new_issue["updated_human"] = ''  # Human readable updated time
            issues_list.append(new_issue)

        # print(issues_list[0])

        # return issues
        return issues_list

    def get_assigned_issues(*args, **kwargs):
        gl = gitlab.Gitlab(host, private_token=kwargs["token"])

        user_id = kwargs["user_id"]
        issues = gl.groups.get(group_id).issues.list()
        
        issues_list = list(filter(lambda x: x._attrs["assignees"] == list(filter(lambda y: y["id"] == user_id, x._attrs["assignees"] )), list(filter(lambda i: i._attrs["assignees"] != [], issues ))))

        return issues_list

    def get_owned_issues(*args, **kwargs):
        gl = gitlab.Gitlab(host, private_token=kwargs["token"])

        user_id = kwargs["user_id"]
        issues = gl.groups.get(group_id).issues.list()
        
        issues_list = (list(filter(lambda x: x._attrs["author"]["id"] == user_id, issues )))

        return issues_list

    def assign_issue_to_user(*args, **kwargs):
        gl = gitlab.Gitlab(host, private_token=kwargs["token"])

        user_id = kwargs["user_id"]
        issue_id = kwargs["issue_id"]
        project_id = kwargs["project_id"]

        project = gl.projects.get(project_id, lazy=True)

        editable_issue_iid = list(filter(lambda x: x._attrs["id"] == issue_id, project.issues.list()))[0].iid
        editable_issue = project.issues.get(issue_id, lazy=True)
        editable_issue._attrs["iid"] = editable_issue_iid
        editable_issue.assignee_ids = [user_id]
        editable_issue.save()

        return editable_issue

    def get_issue(*args, **kwargs):
        """
        Gets issue from Gitlab

        Args:
            **kwargs:
                project_id
                issue_iid = issue's unique id
        """
        token = kwargs["token"]
        gl = gitlab.Gitlab(host, token)

        issue_iid = kwargs['issue_iid']
        id = 35
        project_id = kwargs['project_id']
        # gl = GitlabTools.gl

        project = gl.projects.get(project_id, lazy=True)
        issue = project.issues.get(issue_iid)

        # Issue > Discussions > Notes
        # discussions = issue.discussions.list()
        # for discussion in discussions:
        # for note in discussion.attributes["notes"]:
        # print(note['body'])
        # print(discussions)
        # print(discussions[0].notes)
        discussions = GitlabTools.get_issue_discussion(token,issue)

        return issue, discussions

    def get_issue_discussion(token, issue):
        gl = gitlab.Gitlab(host, private_token=token)

        # project = gl.projects.get(project_id, lazy=True)
        # issue = project.issues.get(issue_iid)

        # Issue > Discussions > Notes
        discussion_list = []
        discussions = issue.discussions.list()
        # print(discussions)
        for discussion in discussions:
            notes = []
            # print(discussion)
            for note in discussion.attributes["notes"]:
                notes.append(note)
                # print(note)
                # data = {
                # 'title': 'foo',
                # 'body': note["body"]
                # }
                # print(note['body'])
            discussion_list.append(notes)
        print(discussion_list)

        return discussion_list

    def get_releases(*args, **kwargs):
        gl = gitlab.Gitlab(host, private_token=kwargs["token"])
        project_id = kwargs["project_id"]

        releases = list(filter(lambda y: y._attrs["id"] == project_id, gl.projects.list() ))[0].releases.list()

        return releases


class StringTools:
    def append_fields(original_text, field_data):
        original_text += "\n"
        for field in field_data:
            md_field = f"**{field}:**"
            # original_text += f'\n{field} {field_data["field"]}'
            original_text += md_field+" "+str(field_data.get(field))+"\n"
            # original_text += field

        return original_text

    def get_datetime():
        pass

    def get_timedelta():
        pass


class Debug:
    def output(*args):
        print("*"*80)
        print("DEBUG")
        print("*"*80)
        for string in args:
            print(string)
        print("*"*80)
