from django.urls import include, path
from .views import *

app_name = 'library'

urlpatterns = [
    # View Parts
    path("", LibraryView.as_view(), name="list"),
    path("projects/<int:project_id>/", ProjectDetail.as_view(), name="project_detail"),

    # View list of issues
    path("request/", IssuesList.as_view(), name="issues_list"),

    # View issue info
    path("request/<int:project_id>/<int:issue_iid>/", BaseIssueDetail.as_view(), name="detail"),
    # path("request/<int:id>/", RequestView.as_view(), name="new_request"),
    # path("request/new_part/", RequestNewPartView.as_view(), name="new_part"),

    # Create new issue
    path("request/new/", BaseIssueCreate.as_view(), name="issue_base"),
    # path("request/make/<int:id>/", BaseIssueCreate.as_view()),
    path("request/make/", NewPartRequestCreate.as_view(), name="issue_new_part"),
    path("request/print/", PrintRequestCreate.as_view(), name="issue_print"),
    # path("request/new_part/", NewPartList.as_view()),
    # path("request/model_test/", RequestNewPartView.as_view(), name="new_part"),
    # path("request/delivery/", DeliveryRequestView.as_view()),
    # path("request/delivery/", DeliveryRequestView2.as_view()),
    # path("request/print_part/", RequestPrintPartView.as_view(), name="print_part"),
    # path("<int:pk>/", DesignDetail.as_view(), name="detail"),
    # path("", DesignList.as_view(), name="list")
]
