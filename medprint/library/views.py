from django.shortcuts import render
from django.views.generic import CreateView, TemplateView, ListView, DetailView, UpdateView, DeleteView
from django.views import View
from django.contrib.auth import get_user_model
import gitlab
import os
from django.shortcuts import render, get_object_or_404, redirect, HttpResponseRedirect
from .utils import *
from .forms import *

User = get_user_model()

class LibraryView(View):
    meta = {
        'title': 'Available Products'
    }

    def get(self, request, *args, **kwargs):
        projects = list(GitlabProject.get_projects())
        return render(request, 'projects/list.html', {'projects': projects, 'meta': self.meta})


class ProjectDetail(DetailView):
    meta = {}

    def get(self, request, *args, **kwargs):
        project = GitlabProject.get_project(kwargs["project_id"])

        return render(request, 'projects/detail.html', {'project': project, 'meta': self.meta})


class RequestView(CreateView):
    template_name = ''
    form_name = ''

    def get(self, request, *args, **kwargs):
        form = self.form_name()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_name(request.POST, request.FILES)
        print(request.FILES)
        if form.is_valid():
            return HttpResponseRedirect('')
        else:
            return render(request, self.template_name, {'form': form})


# class RequestNewPartView(RequestView):
    # template_name = 'requestpart.html'
    # form_name = RequestNewPartForm


class BaseIssueCreate(View):
    form = IssueForm
    meta = {
        'title': 'Create Test Issue',
        'description': 'This is a basic issue form'
    }
    fields = ['title']
    title = "Create a test issue"
    labels = ['basic test']

    def get(self, request, *args, **kwargs):
        form = self.form()
        return render(request, 'issue/new.html', {'form': form, 'title': self.title, 'meta': self.meta})

    def post(self, request, *args, **kwargs):
        form = self.form(request.POST, request.FILES)

        new_issue = GitlabTools.create_issue_from_dict(request.user.gitlab_token,request.POST, self.labels)

        project_id = new_issue.project_id
        issue_iid = new_issue.iid
        url = f"../{project_id}/{issue_iid}"

        return HttpResponseRedirect(url)


class NewPartRequestCreate(BaseIssueCreate):
    form = NewPartRequestForm
    title = "Request a New Product"
    meta = {
        'title': "Request a New Product",
        'description': "Use this form to request a product that is not available in the catalogue"
    }
    labels = ["New Part"]


class PrintRequestCreate(BaseIssueCreate):
    form = PrintRequestForm
    meta = {
        'title': "Print a part",
        'description': "Print a part"
    }
    labels = ["Print Request"]


class DeliveryRequestCreate(BaseIssueCreate):
    form = DeliveryRequestForm
    meta = {
        'title': "Request Delivery",
        'description': "Ask for delivery"
    }
    labels = ["Delivery Request"]


class IssuesList(ListView):
    labels = []
    meta = {
        'title': 'Issues'
    }

    def get(self, request, *args, **kwargs):
        paginate_by = 10
        issue_list = []
        #if request.user.is_authenticated:
        assigned_list = GitlabTools.get_assigned_issues(token=request.user.gitlab_token,user_id=request.user.gitlab_id)
        owned_list = GitlabTools.get_owned_issues(token=request.user.gitlab_token,user_id=request.user.gitlab_id)
        #GitlabTools.get_releases(token=request.user.gitlab_token,project_id=assi)
        release_list = []
        for issue in assigned_list:
            releases = GitlabTools.get_releases(token=request.user.gitlab_token,project_id=issue._attrs["project_id"])
            if releases != []:
                issue.release = releases[0]
            else:
                issue.release = []
            
        context_object_name = 'assigned'
        return render(request, 'issue/list.html', {'assigned': assigned_list, 'owned': owned_list, 'meta': self.meta})


class NewPartList(IssuesList):
    title = "New Part Requests"
    labels = ["New Part"]


class BaseIssueDetail(DetailView):
    title = "Base Issue Detail"

    def get(self, request, *args, **kwargs):
        object = GitlabTools.get_issue(**kwargs, token=request.user.gitlab_token)
        project_id = object[0]._attrs["project_id"]
        releases = GitlabTools.get_releases(token=request.user.gitlab_token,project_id=project_id)
        return render(request, 'issue/detail.html', {'object': object[0], 'releases': releases, 'discussions': object[1], 'title': self.title})
        # return render(request, 'issue/detail.html', {'object': object, 'title': self.title})


# class IssueModelCreate(CreateView):
    # fields = ['desc']
    # model = Issue
    # template_name = 'issue/new.html'
    # form_name = 'IssueModelForm'


# class DeliveryRequestView(CreateView):
    # model = DeliveryRequest
    # fields = ['title', 'item', 'desc', 'quantity']
    # form_name = DeliveryRequestForm
    # template_name = 'issue/delivery.html'

    # def post(self, request, *args, **kwargs):
    # form = DeliveryRequestForm(request.POST)
    # # form.user = request.user
    # if form.is_valid():
    # new_request = form.save(commit=False)
    # new_request.user = request.user
    # new_request.save()
    # return HttpResponseRedirect('')
    # return render(request, self.template_name, {'form': form})


# class DeliveryRequestView2(CreateView):
    # model = DeliveryRequest2
    # fields = ['title', 'item', 'desc', 'quantity']
    # form_name = DeliveryRequestForm2
    # template_name = 'issue/delivery.html'

    # def post(self, request, *args, **kwargs):
    # form = DeliveryRequestForm2(request.POST)
    # # form.user = request.user
    # if form.is_valid():
    # new_request = form.save(commit=False)
    # new_request.user = request.user
    # new_request.save()
    # return HttpResponseRedirect('')
    # return render(request, self.template_name, {'form': form})


class NewIssueView(CreateView):
    template_name = "new_issue.html"
    form_name = "NewIssueForm"
