#!/usr/bin/python3
"""
Script that creates Personal Access Token for Gitlab API;
Tested with:
- GitLab Community Edition 10.1.4
- Gitlab Enterprise Edition 12.6.2
"""
import sys
import lxml
import requests
import os
from urllib.parse import urljoin
from bs4 import BeautifulSoup

endpoint = os.environ.get("GITLAB_HOST")
# endpoint = "https://backend.medprint.org"
root_route = urljoin(endpoint, "/")
sign_in_route = urljoin(endpoint, "/users/sign_in")
pat_route = urljoin(endpoint, "/profile/personal_access_tokens")


def find_csrf_token(text):
    soup = BeautifulSoup(text, "lxml")
    token = soup.find(attrs={"name": "csrf-token"})
    param = soup.find(attrs={"name": "csrf-param"})
    data = {param.get("content"): token.get("content")}
    return data


def obtain_csrf_token():
    r = requests.get(root_route)
    token = find_csrf_token(r.text)
    return token, r.cookies


def sign_in(csrf, cookies, login, password):
    data = {
        "user[login]": login,
        "user[password]": password,
        "user[remember_me]": 0,
        "utf8": "✓"
    }
    data.update(csrf)
    r = requests.post(sign_in_route, data=data, cookies=cookies)
    token = find_csrf_token(r.text)
    return token, r.history[0].cookies


def obtain_personal_access_token(name, expires_at, csrf, cookies):
    data = {
        "personal_access_token[expires_at]": expires_at,
        "personal_access_token[name]": name,
        "personal_access_token[scopes][]": "api",
        "utf8": "✓"
    }
    data.update(csrf)
    r = requests.post(pat_route, data=data, cookies=cookies)
    soup = BeautifulSoup(r.text, "lxml")
    token = soup.find('input', id='created-personal-access-token').get('value')
    return token


def main():
    login = sys.argv[3]
    password = sys.argv[4]

    csrf1, cookies1 = obtain_csrf_token()
    """print("root", csrf1, cookies1)"""
    csrf2, cookies2 = sign_in(csrf1, cookies1, login, password)
    """print("sign_in", csrf2, cookies2)"""

    name = sys.argv[1]
    expires_at = sys.argv[2]
    token = obtain_personal_access_token(name, expires_at, csrf2, cookies2)
    print(token)


if __name__ == "__main__":
    main()
