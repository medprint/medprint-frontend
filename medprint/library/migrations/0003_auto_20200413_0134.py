# Generated by Django 3.0.4 on 2020-04-13 01:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0002_deliveryrequest2'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='deliveryrequest2',
            name='user',
        ),
        migrations.DeleteModel(
            name='DeliveryRequest',
        ),
        migrations.DeleteModel(
            name='DeliveryRequest2',
        ),
    ]
