from django import forms
from django.forms import ModelForm
from .models import *
from .utils import *


class IssueForm(forms.Form):
    project_id = forms.IntegerField()  # Should remove at some point and grab from url
    title = forms.CharField(max_length=250)
    description = forms.CharField(max_length=250)
    email = forms.EmailField(max_length=250)
    # labels = forms.CharField(max_length=250)


# class RequestNewPartForm(IssueForm):
    # # page_title = "Request a New Part"
    # files = forms.FileField()


class NewPartRequestForm(IssueForm):
    files = forms.FileField()
    url = forms.URLField()
    # project_id = forms.IntegerField() # Should pass all of these to a single project that collates parts people need that don't exist


class PrintRequestForm(IssueForm):
    # project_id = forms.IntegerField()
    quantity = forms.IntegerField()
    location = ''


class DeliveryRequestForm(IssueForm):
    pass


#class RequestPrintPartForm(IssueForm):
#    page_title = "Request to Print a Part"
#    choices = GitlabTools.get_designs_list("668hNKkAMHLz2MJmV1bK")
#
#    quantity = forms.IntegerField()
#    design = forms.ChoiceField(choices=choices)


# class IssueModelForm(forms.ModelForm):
    # class Meta:
    # model = Issue
    # fields = [
    # 'desc'
    # ]


# class DeliveryRequestForm(forms.ModelForm):
    # class Meta:
    # model = DeliveryRequest
    # fields = ['item', 'quantity']


# class DeliveryRequestForm2(forms.ModelForm):
    # class Meta:
    # model = DeliveryRequest2
    # fields = ['item', 'quantity']
