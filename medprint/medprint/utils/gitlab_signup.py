#!/usr/bin/python3
"""
Script that creates Personal Access Token for Gitlab API;
Tested with:
- GitLab Community Edition 10.1.4
- Gitlab Enterprise Edition 12.6.2
"""
import sys
import lxml
import requests
import os
from urllib.parse import urljoin
from bs4 import BeautifulSoup

def find_csrf_token(text):
    soup = BeautifulSoup(text, "lxml")
    token = soup.find(attrs={"name": "csrf-token"})
    param = soup.find(attrs={"name": "csrf-param"})
    data = {param.get("content"): token.get("content")}
    return data


def obtain_csrf_token(endpoint):
    root_route = urljoin(endpoint, "/")
    r = requests.get(root_route)
    token = find_csrf_token(r.text)
    return token, r.cookies


def sign_up(endpoint, csrf, cookies, name, username, email, password):
    data = {
        "new_user[name]": name,
        "new_user[username]": username,
        "new_user[email]": email,
        "new_user[email_confirmation]": email,
        "new_user[password]": password,
        "utf8": "✓"
    }
    data.update(csrf)
    sign_up_route = urljoin(endpoint, "/users")
    r = requests.post(sign_up_route, data=data, cookies=cookies)
    token = find_csrf_token(r.text)
    return token, r.history[0].cookies


def obtain_personal_access_token(endpoint,csrf, cookies):
    data = {
        "personal_access_token[expires_at]": "2020-06-30",
        "personal_access_token[name]": "medprint",
        "personal_access_token[scopes][]": "api",
        "utf8": "✓"
    }
    data.update(csrf)
    pat_route = urljoin(endpoint, "/profile/personal_access_tokens")
    r = requests.post(pat_route, data=data, cookies=cookies)
    soup = BeautifulSoup(r.text, "lxml")
    token = soup.find('input', id='created-personal-access-token').get('value')
    return token

def bla():
    print(bla)