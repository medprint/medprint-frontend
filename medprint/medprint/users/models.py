from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import CharField, PositiveIntegerField
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField
from location_field.models.plain import PlainLocationField

user_types = (
    ('hospital', 'Hospital'),
    ('maker', 'Maker'),
    ('deliverer', 'Deliverer'),
    ('dispatcher', 'Dispatcher'),
)


class User(AbstractUser):

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = CharField(_("Display name"), blank=True, max_length=255,
                     help_text="The name you display to the world. Don't include personal information!")
    gitlab_token = CharField(_("GitLab API Token"), blank=False, default="NONE", max_length=255)
    gitlab_id = PositiveIntegerField(_("GitLab User ID"), blank=False, default=1)
    user_type = CharField(max_length=30, choices=user_types, blank=True, null=True)
    image = models.ImageField(_("Profile image"), upload_to="users/profile_pics",
                              height_field=None, width_field=None, max_length=None, null=True, blank=True)

    # Location
    country = CountryField(blank=True, null=True)
    city = CharField(blank=True, null=True, max_length=255)
    location = PlainLocationField(based_fields=['city'], zoom=7,null=True)


    # Metadata
    status_verified = models.BooleanField(default=False, blank=True, null=True)
    status_active = models.BooleanField(default=True, blank=True, null=True)
    timestamp_create = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    timestamp_update = models.DateTimeField(auto_now=True, null=True, blank=True)

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})

    def __str__(self):
        if self.display_name:
            return self.display_name
        else:
            return f"{self.city}, {self.country.name}: {self.user_type}"

    @property
    def display_name(self):
        if self.name:
            return self.name
        else:
            return f"{self.user_type} #{self.pk}"
