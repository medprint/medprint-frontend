from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model

from medprint.users.forms import UserChangeForm, UserCreationForm

User = get_user_model()


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    model = User
    fieldsets = (
        ('User', {'fields': ('name', 'gitlab_token', 'gitlab_id', 'image', 'country', 'city', 'user_type', 'location')}),
    ) + auth_admin.UserAdmin.fieldsets
    list_display = ('username', 'name', 'is_superuser', 'gitlab_token', 'gitlab_id',)
    search_fields = ('name',)
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'name', 'is_superuser', 'gitlab_token', 'gitlab_id', 'country', 'city', 'user_type', 'location', 'password1', 'password2', 'image', 'email')}
         ),
    )
