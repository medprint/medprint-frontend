from django.urls import path

from medprint.users.views import *

app_name = "users"
urlpatterns = [
    path("list/", view=UserListImageView.as_view(), name="list"),
    path("~redirect/", view=user_redirect_view, name="redirect"),
    path("~update/", view=user_update_view, name="update"),
    path("<str:username>/", view=user_detail_view, name="detail"),
]
