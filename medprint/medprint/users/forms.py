from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from medprint.utils.gitlab_signup import *
import gitlab

User = get_user_model()


class UserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User
        fields = ('gitlab_token', 'gitlab_id',)


class UserCreationForm(UserCreationForm):

    error_message = UserCreationForm.error_messages.update(
        {"duplicate_username": _("This username has already been taken.")}
    )

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'email')

    def clean_username(self):
        username = self.cleaned_data["username"]

        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username

        raise ValidationError(self.error_messages["duplicate_username"])

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)

        endpoint = os.environ.get("GITLAB_HOST")

        csrf1, cookies1 = obtain_csrf_token(endpoint)
        """print("root", csrf1, cookies1)"""
        csrf2, cookies2 = sign_up(
            endpoint, csrf1, cookies1, self.cleaned_data["username"], self.cleaned_data["username"], self.cleaned_data["email"], self.cleaned_data["password1"])
        """print("sign_in", csrf2, cookies2)"""
        token = obtain_personal_access_token(endpoint, csrf2, cookies2)

        gl = gitlab.Gitlab(endpoint, private_token=token)
        gl_user = gl.users.list(username=self.cleaned_data["username"])[0]

        user.gitlab_token = token
        user.gitlab_id = gl_user.id

        if commit:
            user.save()
        return user
