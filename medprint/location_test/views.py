from django.shortcuts import render, get_object_or_404, redirect, HttpResponseRedirect
from django.views.generic import CreateView, DetailView
from .models import *


class PlaceDetailView(DetailView):
    model = Place
    template_name = "place/detail.html"


class PlaceCreateView(CreateView):
    template_name = "place_form.html"
    model = Place
    fields = (
        'parent_place',
        'city',
        'location',
    )

    success_url = "/"
