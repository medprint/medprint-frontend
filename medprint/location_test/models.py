from django.db import models
from location_field.models.plain import PlainLocationField


class LocationTest(models.Model):
    city = models.CharField(max_length=255, blank=True, null=True)
    location = PlainLocationField(based_fields=['city'], zoom=7, blank=True, null=True)
    name = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.name


class Place(models.Model):
    parent_place = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )

    name = models.CharField(max_length=250, null=True, blank=True)
    city = models.CharField(max_length=255)
    location = PlainLocationField(based_fields=['city'], zoom=7)

    def __str__(self):
        return self.city
