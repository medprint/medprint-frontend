from django.urls import include, path
from .views import *

app_name = 'location'

urlpatterns = [
    path("", PlaceCreateView.as_view()),
    path("<int:pk>/", PlaceDetailView.as_view()),
]
