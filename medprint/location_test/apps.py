from django.apps import AppConfig


class LocationTestConfig(AppConfig):
    name = 'location_test'
