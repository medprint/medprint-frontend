from django.contrib import admin
from .models import *

# Register your models here.


@admin.register(LocationTest)
class LocationTestAdmin(admin.ModelAdmin):
    class Meta:
        list_display = 'name'


class PlaceInline(admin.TabularInline):
    model = Place
    extra = 0


class PlaceAdmin(admin.ModelAdmin):
    inlines = [
        PlaceInline,
    ]


admin.site.register(Place, PlaceAdmin)
