from django.utils.translation import ugettext_lazy as _
from django.db import models
from config.settings.base import AUTH_USER_MODEL
from django.contrib.auth import get_user_model
from phone_field import PhoneField
from location_field.models.plain import PlainLocationField
from django.contrib.postgres.fields import JSONField
from library.utils import *

User = get_user_model()

makers = User.objects.filter(user_type="maker")
hospitals = User.objects.filter(user_type="hospital")

# Create your models here.

status_choices = (
    ('order_received', 'Order received'),
    ('dispatching_to_maker', 'Dispatching to Maker'),
    ('dispatched_to_maker', 'Dispatched to Maker'),
    ('maker_accepted', 'Maker accepted'),
    ('production_started', 'Production started'),
    ('production_complete', 'Production complete'),
    ('delivery_pending', 'Awaiting Delivery'),
    ('delivery_started', 'Delivery started'),
    ('delivery_received', 'Delivery received'),
    ('cancelled', 'Cancelled'),
    ('error', 'Error'),
)

# Replace with data pulled from Gitlab
item_choices = (
    ('medishield', 'MediShield'),
    ('prusashield', 'Prusa Face Shield'),
    ('mask', 'Mask'),
    ('openpcr', 'OpenPCR')
)


class Order(models.Model):
    item = models.CharField(choices=item_choices, blank=True, null=True, max_length=20)
    quantity = models.IntegerField(default=100)
    success_url = "/"

    # Timestamps to track progress
    status = models.CharField(max_length=50, choices=status_choices, default="order_received")
    timestamp_create = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    timestamp_update = models.DateTimeField(auto_now=True, null=True, blank=True)
    timestamp_dispatch_maker = models.DateTimeField(null=True, blank=True)
    timestamp_maker_accept = models.DateTimeField(null=True, blank=True)
    timestamp_production_start = models.DateTimeField(null=True, blank=True)
    timestamp_production_finish = models.DateTimeField(null=True, blank=True)
    timestamp_delivery_pending = models.DateTimeField(null=True, blank=True)
    timestamp_delivery_start = models.DateTimeField(null=True, blank=True)
    timestamp_delivery_end = models.DateTimeField(null=True, blank=True)
    timestamp_cancel = models.DateTimeField(null=True, blank=True)

    hospital = models.ForeignKey(User, on_delete=models.CASCADE, limit_choices_to={
                                 'user_type': 'hospital'}, related_name='orders_hospital', null=True, blank=True)

    # Filled in when dispatcher accepts order
    dispatcher = models.ForeignKey(User, on_delete=models.CASCADE, limit_choices_to={
                                   'user_type': 'dispatcher'}, related_name="orders_dispatcher", null=True, blank=True)

    # Dispatcher fills in
    maker = models.ForeignKey(User, on_delete=models.CASCADE, limit_choices_to={
                              'user_type': 'maker'}, related_name='orders_maker', null=True, blank=True)
    deliverer = models.ForeignKey(User, on_delete=models.CASCADE, limit_choices_to={
                                  'user_type': 'deliverer'}, related_name="orders_deliverer", null=True, blank=True)
    dispatcher_notes = models.TextField(
        max_length=500, help_text="Visible to all parties in the order", null=True, blank=True)

    # Hospital fills in:
    hospital_phone = PhoneField(blank=True, null=True, help_text="Optional", verbose_name="Phone")
    hospital_notes = models.TextField(
        max_length=500, help_text="Visible to dispatcher and the maker/deliverer assigned to you", null=True, blank=True)

    dropoff_location = PlainLocationField(based_fields=['dropoff_search'], zoom=7, null=True, blank=True)
    dropoff_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Delivery Location Name")
    dropoff_search = models.CharField(
        max_length=250, help_text="Start typing your address or name of your business, and stop when it shows up on the map. We'll reverse engineer your address from there", blank=True, null=True)
    dropoff_json = JSONField(blank=True, null=True, default=dict)

    # Maker fills in:
    maker_phone = PhoneField(blank=True, null=True, help_text="Optional")
    maker_notes = models.TextField(
        max_length=500, help_text="Visible to dispatcher and the maker/deliverer assigned to you", null=True, blank=True)

    pickup_location = PlainLocationField(based_fields=['pickup_search'], zoom=7, null=True, blank=True)
    pickup_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Delivery Location Name")
    pickup_search = models.CharField(
        max_length=250, help_text="Start typing your address or name of your business, and stop when it shows up on the map. We'll reverse engineer your address from there", blank=True, null=True)
    pickup_json = JSONField(blank=True, null=True, default=dict)

    # Deliverer fills in:
    # Do we need anything here?

    def status_verbose(self):
        return dict(status_choices)[self.status]


class SimpleOrder(models.Model):
    item = models.CharField(choices=item_choices, blank=True, null=True, max_length=20)
    product_id = models.IntegerField(_("Product ID"), blank=True, null=True)
    issue_id = models.IntegerField(_("Issue ID"), blank=True, null=True)
    quantity = models.IntegerField(default=100)
    success_url = "/"

    timestamp_create = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    timestamp_update = models.DateTimeField(auto_now=True, null=True, blank=True)
    status = models.CharField(max_length=50, choices=status_choices, default="order_received", null=True, blank=True)

    test_user = models.ForeignKey(User, on_delete=models.CASCADE,
                                  related_name='simple_orders_test_user', null=True, blank=True)

    hospital = models.ForeignKey(User, on_delete=models.CASCADE,
                                 related_name='simple_orders_hospital', null=True, blank=True)

    dispatcher = models.ForeignKey(User, on_delete=models.CASCADE,
                                   related_name="simple_orders_dispatcher", null=True, blank=True)

    maker = models.ForeignKey(User, on_delete=models.CASCADE, related_name='simple_orders_maker', null=True, blank=True)

    deliverer = models.ForeignKey(User, on_delete=models.CASCADE,
                                  related_name="simple_orders_deliverer", null=True, blank=True)

    hospital_notes = models.TextField("Your notes",
                                      max_length=500, help_text="Visible to dispatcher and the maker/deliverer assigned to you", null=True, blank=True)

    maker_notes = models.TextField(
        max_length=500, help_text="Visible to dispatcher and the maker/deliverer assigned to you", null=True, blank=True)

    dispatcher_notes = models.TextField(
        max_length=500, help_text="Visible to everyone involved in the order", null=True, blank=True)

    def status_verbose(self):
        return dict(status_choices)[self.status]

    def __str__(self):
        return f"{self.quantity} x {self.item}"

    @property
    def product_name(self):
        product = GitlabProject.get_project(self.product_id)
        return product.name

    @property
    def product(self):
        if self.product_id:
            product = GitlabProject.get_project(self.product_id)
        else:
            product = {
                'id': 0,
                'name': 'No product selected'
            }
        return product
