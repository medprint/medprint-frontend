import gitlab
from django.shortcuts import render, get_object_or_404, redirect, HttpResponseRedirect
from django.views.generic import CreateView, DetailView, UpdateView, ListView, DeleteView
from .models import *
from .forms import *
from django.contrib.auth import get_user_model
from library.utils import *
User = get_user_model()

host = os.environ.get("GITLAB_HOST")
group_id = os.environ.get("GITLAB_GROUP_ID")
gl = gitlab.Gitlab(host)


class OrderView(CreateView):
    model = Order
    fields = [
        'hospital',
    ]


# class HospitalOrder(CreateView):
    # form = HospitalOrderForm
    # model = Order
    # template_name = 'hospital.html'
    # fields = [
    # 'item',
    # 'quantity',
    # 'dropoff_location',
    # 'dropoff_name',
    # 'dropoff_search',
    # 'hospital_phone'
    # ]

    # success_url = "/"


class OrderList(ListView):
    model = Order
    template_name = 'order_list.html'


class OrderDetail(DetailView):
    model = Order
    template_name = 'order_detail.html'


class OrderUpdate(UpdateView):
    model = Order


class DispatchOrder(UpdateView):
    model = Order
    form = DispatcherForm
    template_name = "order_dispatch.html"
    fields = [
        'maker',
        'deliverer',
        'dispatcher_notes',
    ]


class MakerForm(UpdateView):
    model = Order
    form = DispatcherForm
    template_name = "order_form.html"
    fields = [
        'pickup_search',
        'pickup_location',
        'maker_phone',
        'maker_notes',
    ]
    success_url = "/"


class SimpleOrderDetail(DetailView):
    model = SimpleOrder
    template_name = 'simple/detail.html'

    def get(self, request, *args, **kwargs):
        context = {}
        if 'pk' in self.kwargs:
            order_id = self.kwargs["pk"]
            order = SimpleOrder.objects.get(pk=order_id)

        context = {'object': order}
        return render(request, self.template_name, context)


class SimpleHospitalOrder(CreateView):
    """
    First step of the process. Hospital creates a new order
    """
    form = SimpleHospitalOrderForm
    model = SimpleOrder
    template_name = 'simple/hospital_form.html'
    fields = [
        'item',
        'quantity',
        'hospital_notes',
        'product_id',
    ]

    def post(self, request, *args, **kwargs):
        form = SimpleHospitalOrderForm(request.POST)
        print(request.POST)
        if form.is_valid():
            #print("\n\n\n\n\nKWARGS:\n")
            #print(kwargs)
            #print("\n\n\n\n\nFORM.INSTANCE:\n")
            #print(form.instance)
            #print("\n\n\n\n\nFORM.INSTANCE.PRODUCT_ID:\n")
            #print(form.instance.product_id)
            #print("\n\n\n\n\nFORM.INSTANCE.HOSPITAL_NOTES:\n")
            #print(form.instance.hospital_notes)
            title = "Order of " + str(form.instance.quantity) + " " + str(form.instance.product_name)
            description = str(form.instance.quantity) + " x " + str(form.instance.product_name) + "Notes: " + form.instance.hospital_notes
            project_id = GitlabProject.get_project(form.instance.product_id).id
            issue = GitlabTools.create_issue(token=request.user.gitlab_token,project_id=project_id,title=title,description=description,labels=['basic'])
            form.instance.issue_id = issue.id

            releases = GitlabTools.get_releases(token=request.user.gitlab_token,project_id=project_id)

            new_order = form.save(commit=False)
            new_order.hospital = request.user
            new_order.save()
        else:
            print(form.errors)

        return HttpResponseRedirect(f"/orders/simple/{new_order.pk}")

    def get(self, request, *args, **kwargs):
        initial_data = {}
        context = {}
        initial_data['quantity'] = 500
        initial_data['hospital_notes'] = "foo"
        if 'product_id' in self.kwargs:
            token = request.user.gitlab_token
            product = GitlabProject.get_project(kwargs["product_id"])
            initial_data["product_id"] = kwargs["product_id"]
            context["product"] = product

        form = self.form(initial=initial_data)

        # context = {'form': form, 'fields': self.fields}
        context['form'] = form
        context['fields'] = self.fields

        return render(request, self.template_name, context)


class SimpleOrderDispatch(UpdateView):
    """
    Step 2: Dispatcher accepts order and passes on to maker
    """
    print("SimpleOrderDispatch")

    form = SimpleOrderDispatchForm

    model = SimpleOrder
    template_name = 'simple/dispatch_form2.html'
    fields = [
        'quantity',
        'item',
        'hospital',
        'maker',
        'maker_notes',
        'deliverer',
        'status',
        'dispatcher_notes',
    ]
    success_url = '/'
    
    def get_context_data(self, **kwargs):
        print("get_context_data")
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['maker_list'] = User.objects.filter(user_type="maker")
        context['deliverer_list'] = User.objects.filter(user_type="deliverer")
        print(context)
        return context

    def form_valid(self, form):
        print("form_valid")
        redirect_url = super(SimpleOrderDispatch, self).form_valid(form)
        order = SimpleOrder.objects.get(pk=self.kwargs['pk'])
        order.dispatcher = self.request.user
        order.status = "dispatched_to_maker"
        order.maker_list =  User.objects.filter(user_type="maker")
        print(order.maker_list)
        order.save()
        return HttpResponseRedirect(f"/orders/simple/")

    def post(self, request, *args, **kwargs):
        print("post")
        order = SimpleOrder.objects.get(pk=self.kwargs['pk'])
        request.POST = request.POST.copy()
        print(request.POST)
        order.test_user = request.user
        order.dispatcher = request.user
        order.dispatcher_notes = request.POST["dispatcher_notes"]
        order.deliverer = User.objects.get(pk=request.POST["deliverer"])
        order.maker = User.objects.get(pk=request.POST["maker"])
        order.maker_list =  User.objects.filter(user_type="maker")
        print(order.maker_list)
        GitlabTools.assign_issue_to_user(token=request.user.gitlab_token,user_id=order.maker.gitlab_id, issue_id=order.issue_id, project_id=order.product_id)

        if 'mark_as_done' in request.POST:
            print("DONE!!!!!!!")
            order.status = 'production_complete'
            # Send Signal to dispatcher here
        elif 'accept_order in request.POST':
            order.status = 'maker_accepted'
        order.save()
        return super(SimpleOrderDispatch, self).post(request, **kwargs)


class SimpleOrderMaker(UpdateView):
    """
    Step 3: Maker accepts order
    """
    form = SimpleMakerForm
    model = SimpleOrder
    template_name = 'simple/maker_form.html'
    fields = ["maker_notes"]
    success_url = "/"

    # def form_valid(self, form):
    # print(form)
    # print(self.request.post)
    # # if 'update_order' in self.request.post:
    # # print("Update order")
    # # elif 'mark_as_done' in self.request.post:
    # # print("Mark as done")
    # # else:
    # # print("Text not found")
    # redirect_url = super(SimpleOrderMaker, self).form_valid(form)
    # order = SimpleOrder.objects.get(pk=self.kwargs['pk'])
    # order.maker = self.request.user
    # order.status = 'maker_accepted'
    # order.save()
    # return HttpResponseRedirect(f"/orders/simple/")
    # return HttpResponseRedirect(f"/orders/simple/{order.pk}")

    def post(self, request, *args, **kwargs):
        order = SimpleOrder.objects.get(pk=self.kwargs['pk'])
        request.POST = request.POST.copy()
        order.test_user = request.user
        order.maker = request.user
        if 'mark_as_done' in request.POST:
            print("DONE!!!!!!!")
            order.status = 'production_complete'
            # Send Signal to dispatcher here
        elif 'accept_order in request.POST':
            order.status = 'maker_accepted'
        order.save()
        return super(SimpleOrderMaker, self).post(request, **kwargs)


class SimpleOrderMakerComplete(UpdateView):
    """
    Step 4. Maker marks production complete
    """
    form = SimpleMakerForm
    model = SimpleOrder
    template_name = 'simple/maker_form.html'
    fields = []
    success_url = "/"

    def form_valid(self, form):
        redirect_url = super(SimpleOrderMaker, self).form_valid(form)
        order = SimpleOrder.objects.get(pk=self.kwargs['pk'])
        # order.maker = self.request.user
        order.status = 'production_complete'
        # Somehow signal dispatcher for next step
        order.save()
        return HttpResponseRedirect(f"/orders/simple/")
# Lists


class SimpleOrderList(ListView):
    # model = SimpleOrder
    queryset = SimpleOrder.objects.all().order_by('-timestamp_update')
    template_name = 'simple/list.html'


class UserOrderList(ListView):
    template_name = 'simple/list.html'

    def get_queryset(self):
        user_is_maker = SimpleOrder.objects.filter(maker=self.request.user)
        user_is_hospital = SimpleOrder.objects.filter(hospital=self.request.user)
        user_is_deliverer = SimpleOrder.objects.filter(deliverer=self.request.user)
        queryset = user_is_maker | user_is_hospital | user_is_deliverer
        queryset = queryset.order_by('-timestamp_update')

        return queryset
