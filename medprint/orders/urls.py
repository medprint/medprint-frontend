from django.urls import include, path
from .views import *

app_name = 'orders'

urlpatterns = [
    # path("simple/new/", SimpleHospitalOrder.as_view(), name="new"),
    path("simple/new/<int:product_id>/", SimpleHospitalOrder.as_view(), name="new"),
    path("simple/all/", SimpleOrderList.as_view(), name="list"),
    path("simple/", UserOrderList.as_view(), name="me"),
    path("simple/<int:pk>/", SimpleOrderDetail.as_view(), name="detail"),
    path("simple/<int:pk>/dispatch/", SimpleOrderDispatch.as_view(), name="dispatch"),
    path("simple/<int:pk>/maker/", SimpleOrderMaker.as_view(), name="maker"),

    # path("new/", HospitalOrder.as_view(), name="new"),
    # path("", OrderList.as_view(), name="list"),
    # path("<int:pk>/", OrderDetail.as_view(), name="list"),
    # path("<int:pk>/dispatch/", DispatchOrder.as_view(), name="dispatch"),
    # path("<int:pk>/make/", MakerForm.as_view(), name="make"),
    # path("hospitals/", UserList_Hospital.as_view(), name="hospitals"),
    # path("deliverers", UserList_Deliverer.as_view(), name="deliverers"),
    # path("dispatchers/", UserList_Dispatcher.as_view(), name="dispatchers"),
    # path("<int:pk>/", UserDetail.as_view(), name="detail"),
]
