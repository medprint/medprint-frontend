from django import forms
from .models import *


class HospitalOrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = [
            "hospital_notes"
        ]


class DispatcherForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = []


class SimpleHospitalOrderForm(forms.ModelForm):
    class Meta:
        model = SimpleOrder
        exclude = []


class SimpleOrderDispatchForm(forms.ModelForm):
    class Meta:
        model = SimpleOrder
        exclude = []


class SimpleMakerForm(forms.ModelForm):
    class Meta:
        model = SimpleOrder
        exclude = []
