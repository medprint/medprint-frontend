from django.db import models
from config.settings.base import AUTH_USER_MODEL
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_countries.fields import CountryField

User = AUTH_USER_MODEL

user_types = (
    ('hospital', 'Hospital'),
    ('maker', 'Maker'),
    ('deliverer', 'Deliverer'),
    ('dispatcher', 'Dispatcher'),
)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")
    user_type = models.CharField(max_length=30, choices=user_types, blank=True, null=True)

    # Location
    country = CountryField(blank=True, null=True)

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
