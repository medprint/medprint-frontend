from django.views import View
import json
from django.shortcuts import render, get_object_or_404, redirect, HttpResponseRedirect
from django.views.generic import CreateView, DetailView, UpdateView, ListView, DeleteView
from .models import *
from .forms import *
from geopy.geocoders import Nominatim
from django.contrib import messages
from django.contrib.auth import get_user_model

User = get_user_model()


class UserList(ListView):
    template_name = "list.html"
    model = User


class UserList_Maker(UserList):
    queryset = User.objects.filter(user_type="maker")


class UserList_Hospital(UserList):
    queryset = User.objects.filter(user_type="hospital")


class UserList_Deliverer(UserList):
    queryset = User.objects.filter(user_type="deliverer")


class UserList_Dispatcher(UserList):
    queryset = User.objects.filter(user_type="dispatcher")


class UserDetail(DetailView):
    template_name = "detail.html"
    model = User

# class UserProfileUpdate(UpdateView):
# model = UserProfile
# template_name = 'update.html'
# form = UserProfileForm
# fields = [
# "user_type",
# "country",
# ]


# class Update(View):
# form = UserProfileForm

# def get(self, request, *args, **kwargs):
# user = request.user
# form = self.form(initial={'user_type': user.user_type})
# template_name = "update.html"

# context = {"form": self.form}
# return render(request, "update.html", context)

# def post(self, request, *args, **kwargs):
# user = request.user
# formdata = request.POST
# form = self.form(request.POST)
# print(form)
# print(user.profile)
# if form.is_valid():
# print("form ok")
# print(user)
# user.profile.country = request.POST('country')
# user.save()


# def profile_edit(request):
# user = request.user
# form = UserProfileForm(request.POST or None, initial={'name': user.name,
# 'surname': user.surname})
# if request.method == 'POST':
# if form.is_valid():
# # user.student.name = request.POST['name']
# # user.student.surname = request.POST['surname']

# user.save()
# return HttpResponseRedirect('index')

# context = {
# "form": form
# }

# return render(request, "update.html", context)
