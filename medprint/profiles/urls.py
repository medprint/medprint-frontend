from django.urls import include, path
from .views import *

app_name = 'profile'

urlpatterns = [
    path("all/", UserList.as_view(), name="all"),
    path("makers/", UserList_Maker.as_view(), name="makers"),
    path("hospitals/", UserList_Hospital.as_view(), name="hospitals"),
    path("deliverers", UserList_Deliverer.as_view(), name="deliverers"),
    path("dispatchers/", UserList_Dispatcher.as_view(), name="dispatchers"),
    path("<int:pk>/", UserDetail.as_view(), name="detail"),
]
