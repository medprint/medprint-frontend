from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
User = get_user_model()


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('gitlab_username', nargs="+", type=str)
        parser.add_argument('gitlab_password', nargs="+", type=str)
        parser.add_argument('gitlab_token', nargs="+", type=str)

    def handle(self, *args, **options):
        gitlab_id = 1
        username = options['gitlab_username'][0]
        password = options['gitlab_password'][0]
        token = options['gitlab_token'][0]

        user_info = {
            'gitlab_id': 1,
            'username': options['gitlab_username'][0],
            'password': options['gitlab_password'][0],
            'token': options['gitlab_token'][0],
        }

        # self.stdout.write(self.style.SUCCESS(user_info))
        # # self.stdout.write(self.style.SUCCESS('Printing arguments'))
        # self.stdout.write(self.style.SUCCESS(gitlab_id))
        # self.stdout.write(self.style.SUCCESS(options['gitlab_username']))
        # self.stdout.write(self.style.SUCCESS(options['gitlab_password']))
        # self.stdout.write(self.style.SUCCESS(options['gitlab_token']))
        # self.stdout.write(self.style.SUCCESS(User.objects.all()))

        root = User.objects.create_superuser(
            username=user_info['username'],
            password=user_info['password'],
            gitlab_token=user_info['token'],
            name="Root user"
        )

        # self.stdout.write(self.style.SUCCESS(root))
        # self.stdout.write(self.style.SUCCESS("Created superuser"))

        # print(User.objects.all())
