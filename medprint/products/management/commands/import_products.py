from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from ... models import *
import os
import gitlab
User = get_user_model()

host = os.environ.get("GITLAB_HOST")
group_id = os.environ.get("GITLAB_GROUP_ID")
gl = gitlab.Gitlab(host)


class Command(BaseCommand):
    """"
    Import all repositories as product objects
    """

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        print(Product.objects.all())
        group = gl.groups.get(group_id)
        projects = group.projects.list()
        for project in projects:
            print(project)
            # if Product.objects.get(project_id=project.id):
            # print(f"Project {project.name} already exists. Skipping")
            # pass
            # else:
            print(f"Creating product {project.name}")
            new_product = Product(
                name=project.name,
                project_url=project.web_url,
                project_id=project.id,
                avatar_url=project.avatar_url,
                description=project.description,
            )
            new_product.save()
        print(Product.objects.all())
