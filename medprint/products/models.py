from django.db import models
from django.utils.translation import ugettext_lazy as _
import os
import gitlab

host = os.environ.get("GITLAB_HOST")
group_id = os.environ.get("GITLAB_GROUP_ID")
gl = gitlab.Gitlab(host)

# Create your models here.


class Product(models.Model):
    project_url = models.URLField(_("Project URL"), max_length=200)
    project_id = models.IntegerField(_("Project ID"))
    description = models.TextField(_("Description"))

    avatar_url = models.URLField(_("Avatar URL"), max_length=200, blank=True, null=True)
    product_updated = models.DateTimeField(_("Updated timestamp"), auto_now=True)

    name = models.CharField(_("Name"), max_length=200)

    def __str__(self):
        return self.name

    def update_from_repo(self, *args, **kwargs):
        group = gl.groups.get(group_id)
        projects = group.projects.list()

        for project in projects:
            if project.id == self.project_id:
                self.description = project.description
                self.avatar_url = project.avatar_url
                self.name = project.name

        self.save()
