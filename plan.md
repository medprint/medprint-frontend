# Medprint

## Description

Medprint wants to counter the supply shortages of essential medical devices caused by the current Covid 19 pandemic. We bring hospitals together with the decentralized manufacturing capacities of the maker community.

Are you a medical practitioner and need more auxiliary materials than your suppliers can afford?

Do you or your institution own one or more 3D printers that are not currently being used?

Do you have a car and robust health and would you like to make yourself useful by picking up the printed components and delivering them to hospitals?

Can you support with requirements management and construction of other required components?

Then register now and help where it is urgently needed!

## Questions

* Are we talking about a pre-selected catalog of components? Or a wide range of PPI, etc in general?
* Do we need hospitals to prove they're really hospitals?
* Do makerspaces need any sort of certification (I'd guess no)
* Can makerspaces/deliverers charge? Even if just to cover costs
* Do we also need 3D designers to handle new use cases/improve existing designs?
* Do all 3D designs need to be open-source? Are we open to free-as-in-beer designs?
* Geographical coverage - worldwide vs regional isn't very different technically
* Localization?

## Website

### Users

* Hospitals
* Makers - individual
* Makers - institutions
* Deliverers
* Others?

Make base user class, with fields:

* Location: Django location field
* Name: Charfield
* Contact info: Phone/email fields
* Timestamp
* Additional contact people: Textarea

Hospitals
  - Need:
    - Item: Char? Tag? (tags allow easier browsing and search)
    - Quantity: Int
    - Urgency: Selectbox
    - Do as a formset

Makers
  - Equipment
  - Materials
  - Capacity
  - Certification?

Deliverers
  - Availability
  - Capacity

### Models

#### Base Model

* Created: Timestamp
* Updated: Timestamp
* Author: ForeignKey - user

#### Needed Item

e.g. mask, ventilator, etc

* Title - auto-generated from location/item/qty
* Name: Tag field
* Quantity: Int
* Pic: Image
* Files: e.g. specs pdf. Filefield
* Urgency - selectbox
* Notes - TextArea
* Captcha
* Status - fulfilled or not?
* Comments? To clarify etc
* Contact people

#### Makerspace

* Title - auto-generated
* Equipment - from list of tags
* Capacity - can do x things per day
* Pics
* Files
* Notes
* Captcha
* Comments?
* Contact people
* Open hours
* Skills

#### 3D Design

* Title: Charfield
* Tags
* STL
* Images
* Files - specs pdf, etc
* Description

#### Order

* ID
* Supplier - should be tied to logged in user
* Recipient
* Deliverer
* Item details
* Status - how to easily update this?
* Timestamp fields -
  * order made - automatic
  * maker finishes making - maker hits button
  * deliver picks up - deliverer hits button
  * deliverer drops off - deliverer
  * hospital confirms received - hospital

### Pages

* Who needs supplies - map + table. Sort by proximity to user + urgency?
* Who has equipment - map + table, like above
* About - TemplateView
* Orders - secure this. Only allow orderer and relevant makerspace to see
* FAQ - ListView - let users add questions
* Sign up pages - Hospital/Maker/Deliverer/Other

### Workflow

1. Hospital posts stuff it needs
2. Makers respond, saying I can provide this (or x units of this)
3. Hospital confirms order
4. Maker makes
5. Maker contacts deliverer
6. Deliverer picks up package
7. Deliverer drops off package at hospital

Various status updates happen along the way to ensure everyone is on same page
