# Django-Medprint

## What is it?

A front-end for medprint.org

## Get Started

### Recommended Method

Follow [these instructions](https://gitlab.com/medprint/medprint-deploy)

### Running for Development Purposes

#### Prerequisites

* Postgres database
* A Gitlab instance to connect to
* [Pipenv](https://github.com/pypa/pipenv)

#### Set up environment variables

In your shell, create environment variables for the following:

* GITLAB_HOST
* GITLAB_TOKEN
* GITLAB_GROUP_ID

You can do this via `export <VARIABLE_NAME>="VARIABLE_VALUE"`

#### Install required Python modules

```pipenv install```

#### Start Python shell

```pipenv shell```

#### Make Migrations

```
./manage.py makemigrations
./manage.py migrate
```

#### Run Server

```
./manage.py runserver 7000
```

## Access Development Site

In your browser, open http://127.0.0.1:7000

## How Do I?

For all of the below, you'll need to be in the pipenv. To do this run ```pipenv shell```

### Create an Admin user

```
./manage.py createsuperuser
```

### The database has gone to Hell!

```
django-admin flush
```
